# friends.computer website

[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

## Getting started

This repository uses [git submodules][submodule], so make sure to
clone with `git clone --recurse-submodules …` or use `git submodule
init && git submodule update` if you already cloned.

Next, install [hugo][hugo], then run `hugo server
--disableFastRender`.

[hugo]: https://gohugo.io/
[submodule]: https://git-scm.com/book/en/v2/Git-Tools-Submodules


## License

CC BY-SA-4.0. See [LICENSE.txt](./LICENSE.txt).
